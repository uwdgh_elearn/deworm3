Global Health child-theme using the UW Boundless WordPress theme.

This theme requires the UW 2014 WordPress theme:
https://github.com/uweb/uw-2014

The UW Slideshow template that is part of this child-theme requires the uw-slideshow plugin: 
https://github.com/uweb/uw-slideshow
