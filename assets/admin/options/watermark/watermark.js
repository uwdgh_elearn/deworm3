// The document ready event executes when the HTML-Document is loaded
// and the DOM is ready.
jQuery(document).ready(function( $ ) {

  //add watermark to DOM
  var text = $( '[name="uwdgh_theme_show_watermark_text"]' ).val();
  $('#uw-container-inner').prepend('<div class="container row"><div class="col-lg-12"><div class="watermark ribbon"><span>'+text+'</span></div></div></div>');

  //event
  $(document).on('click', '.watermark.ribbon', function(e){
    $(this).hasClass("right") ? $(this).removeClass("right") : $(this).addClass("right") ;
  });


})

// The window load event executes after the document ready event,
// when the complete page is fully loaded.
jQuery(window).load(function () {

})
