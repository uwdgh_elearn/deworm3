<?php
/**
* Class with functionality for the template-slideshow-hero.php template
*/
if ( !class_exists( 'UWDGH_Template_Slideshow' ) ) {

	class UWDGH_Template_Slideshow {

		/**
    * class initializaton
    */
    public static function init() {
      // add metaabox
      add_action( 'add_meta_boxes_page',  array( __CLASS__, 'add_meta_box' ));
      // save metabox action
      add_action( 'publish_page',  array( __CLASS__, 'save_custom_post_meta' ));
      add_action( 'draft_page',  array( __CLASS__, 'save_custom_post_meta' ));
      add_action( 'future_page',  array( __CLASS__, 'save_custom_post_meta' ));
      add_action( 'pending_page',  array( __CLASS__, 'save_custom_post_meta' ));
      add_action( 'private_page',  array( __CLASS__, 'save_custom_post_meta' ));
    }

	  /**
	   * Add custom metaboxes
	   * @param $post
	   */
	  static function add_meta_box( $post ) {
	    // Get the page template post meta
	    $page_template = get_post_meta( $post->ID, '_wp_page_template', true );
	    // If the current page uses our specific
	    // template, then output our custom metabox
	    if ( 'templates/template-slideshow-hero.php' == $page_template ) {
        if (is_plugin_active('uw-slideshow/class.uw-slideshow.php') ) {
          add_meta_box(
            'dgh-slideshow-shortcode-metabox', // Metabox HTML ID attribute
            'Slideshow Hero', // Metabox title
            array( __CLASS__, 'slideshow_shortcode_metabox' ), // callback name
            'page', // post type
            'side', // context (advanced, normal, or side)
            'core' // priority (high, core, default or low)
          );
        } else {
          add_action( 'admin_notices',  array( __CLASS__, 'admin_notice_slideshow_plugin__warning' ));
        }
	    }
	  }

	  // Define the meta box form fields here
	  static function slideshow_shortcode_metabox() {
	    global $post;
	    wp_nonce_field( 'slideshow_shortcode_save', 'slideshow_shortcode_nonce' );
		  ?>
		  <label for="slideshow_id"><?php _e('Slideshow ID', 'uwdgh'); ?>:</label><br>
		  <input class="" type="number" name="slideshow_id" id="slideshow_id" value="<?php echo esc_attr( get_post_meta( $post->ID, 'slideshow_id', true ) ); ?>" min="1" max="2147483647" /><br><br>
			<input class="" type="checkbox" name="slideshow_simple" id="slideshow_simple" <?php if( get_post_meta( $post->ID, 'slideshow_simple', true) == true ) { ?>checked="checked"<?php } ?> />&nbsp;<label for="slideshow_simple"><?php _e('Simple photo-slider', 'uwdgh'); ?></label><br>
		  <input class="" type="checkbox" name="slideshow_no_title" id="slideshow_no_title" <?php if( get_post_meta( $post->ID, 'slideshow_no_title', true) == true ) { ?>checked="checked"<?php } ?> />&nbsp;<label for="slideshow_no_title"><?php _e('Hide page title', 'uwdgh'); ?></label><br>
		  <p><a href="<?php echo admin_url( 'edit.php?post_type=slideshow', 'https' ); ?>"><?php _e('Go to Slideshows', 'uwdgh'); ?></a></p>
		  <?php
	  }

	  // Save metabox POST values
	  static function save_custom_post_meta() {
	    global $post;
	    // Sanitize/validate post meta here, before calling update_post_meta()
	    if ( ! isset($_POST['slideshow_shortcode_nonce'])){
	        return;
	    }
	    if ( ! wp_verify_nonce( $_POST['slideshow_shortcode_nonce'], 'slideshow_shortcode_save' ) ) {
	        return;
	    }

	    //$input = sanitize_text_field( $_POST['slideshow_shortcode'] );
	    //update_post_meta( $post->ID, 'slideshow_shortcode', $input );
	    update_post_meta( $post->ID, 'slideshow_id', $_POST['slideshow_id'] );
	    update_post_meta( $post->ID, 'slideshow_simple', $_POST['slideshow_simple'] );
	    update_post_meta( $post->ID, 'slideshow_no_title', $_POST['slideshow_no_title'] );
	  }

	  //admin notice
	  static function admin_notice_slideshow_plugin__warning() {
		  ?>
		  <div class="notice notice-warning is-dismissible">
		      <p><?php _e( 'The <a href="https://github.com/uweb/uw-slideshow" target="_blank">uw-slideshow</a> plugin is required with the current template.', 'default' ); ?></p>
		  </div>
		  <?php
	  }

	}

	UWDGH_Template_Slideshow::init();
}
